Source: emacs-bazel-mode
Section: editors
Priority: optional
Maintainer: Debian Emacsen team <debian-emacsen@lists.debian.org>
Uploaders: Xiyue Deng <manphiz@gmail.com>
Build-Depends: debhelper-compat (= 13),
 dh-elpa,
 git,
Standards-Version: 4.7.0
Homepage: https://github.com/bazelbuild/emacs-bazel-mode
Vcs-Browser: https://salsa.debian.org/emacsen-team/emacs-bazel-mode
Vcs-Git: https://salsa.debian.org/emacsen-team/emacs-bazel-mode.git
Testsuite: autopkgtest-pkg-elpa
Rules-Requires-Root: no

Package: elpa-bazel-mode
Architecture: all
Depends: ${elpa:Depends}, ${misc:Depends}
Recommends: emacs
Enhances: emacs
Description: Bazel support for GNU Emacs
 This repository provides support for Bazel in GNU Emacs. It consists of
 a single file, bazel.el, which only depends on GNU Emacs and not on
 other libraries.
 .
 The library provides major modes for editing Bazel BUILD files,
 WORKSPACE files, .bazelrc files, as well as Starlark files. It also
 provides commands to run Bazel commands and integration with core GNU
 Emacs infrastructure like compilation and xref.
